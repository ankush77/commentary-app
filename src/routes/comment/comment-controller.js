const router = require('express').Router();
const commentService = require('./comment-service')

router.post("/add", add);
router.get("/", getAll);

async function add(req, res, next) {
    await commentService.add(req.body).then(result =>
        res.status(200).json({ 'qliktext': 'comment added successfully' })
    )
        .catch(error => res.status(400).send("unable to save to database"));
}
async function getAll(req, res, next) {
    await commentService.getAll().then(result => res.json(result))
        .catch(error => res.status(404).send(error));
}

module.exports = router;