const commentRepository = require('./comment-repository');

async function add(data) {
    let timestamp = new Date();
    data.timestamp = timestamp.toString();
    data.current_filters = JSON.stringify(data.current_filters);
    data.table_data = JSON.stringify(data.table_data);
    return await commentRepository.add(data);
}
async function getAll() {
    return await commentRepository.getAll().then(result => {
        let tableData = result.map(data => {
            data.current_filters = JSON.parse(data.current_filters);
            data.table_data = JSON.parse(data.table_data);
            return data;
        });
        return tableData
    }).catch(error => {
        return Promise.reject(error);
    });
}


module.exports = {
    add,
    getAll
}