const poolPromise = require('../../database/index');
const sql = require('mssql')

async function add(data) {
    var query = "INSERT INTO comment (CommentText, CurrentUser,Timestamp,CurrentFilters,TableData,RowId,ObjectId) VALUES (@commentText,@currentUser,@timeStamp,@currentFilters,@tableData,@rowId,@objectId)";
    try {
        const pool = await poolPromise
        const result = await pool.request()
            .input('commentText', sql.VarChar, data.comment_text)
            .input('currentUser', sql.VarChar, data.current_user)
            .input('timeStamp', sql.VarChar, data.timestamp)
            .input('currentFilters', sql.VarChar, data.current_filters)
            .input('tableData', sql.VarChar, data.table_data)
            .input('rowId', sql.VarChar, data.row_id)
            .input('objectId', sql.VarChar, data.object_id)
            .query(query);
        return result;
    } catch (err) {
        return Promise.reject(err.message);
    }

}

async function getAll() {
    var query = "SELECT CommentText as 'comment_text',CurrentUser as 'current_user',Timestamp as 'timestamp',CurrentFilters as 'current_filters',TableData as 'table_data',RowId as 'row_id',ObjectId as 'object_id' FROM comment";
    try {
        const pool = await poolPromise
        const result = await pool.request()
            .query(query);
        return result.recordset;
    } catch (err) {
        return Promise.reject(err.message);
    }
}

module.exports = {
    add,
    getAll
}