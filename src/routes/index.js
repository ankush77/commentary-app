function setRoute(app) {
    app.use("/qlikext", require("./comment/comment-controller"));
};

module.exports = {
    setRoute
};