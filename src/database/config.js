const HOST = "127.0.0.1";
const USERNAME = "sa";
const PASSWORD = "root";
const DATABASE_NAME = "comment";
const INSTANCE_NAME = "MSSQLSERVER01";
const PORT = 1433;

const dbConfig = {
    user: USERNAME,
    password: PASSWORD,
    server: HOST,
    database: DATABASE_NAME,
    options: {
        trustedconnection: true,
        enableArithAbort: true,
        instancename: INSTANCE_NAME
    },
    port: PORT
}

module.exports = dbConfig;