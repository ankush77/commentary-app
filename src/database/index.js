const sql = require('mssql')
const dbConfig = require('./config');

const poolPromise = new sql.ConnectionPool(dbConfig).connect()
    .then(pool => {
        console.log('Connected successfully to MSSQL')
        return pool;
    })
    .catch(err => console.log('Database Connection Failed!', err))


module.exports = poolPromise;


